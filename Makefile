EXCLUDE = libuv

all:
	@if [ ! -d libuv ]; then \
		/bin/bash ./libuv_init.sh; \
	fi
	@for dir in $$(ls -d */ | grep -v ${EXCLUDE}); do make -C $${dir}; done; \

clean:
	for dir in $$(ls -d */ | grep -v ${EXCLUDE}); do make -C $${dir} clean; done

clean-all: clean
	-rm -rf libuv

.PHONY: all clean clean-all
