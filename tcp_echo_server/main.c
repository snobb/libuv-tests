/*
 *  main.c
 *  author: Aleksei Kozadaev (2015)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uv.h>

#define DEFAULT_PORT 7777
#define DEFAULT_BACKLOG 10

uv_loop_t *loop = NULL;
struct sockaddr_in addr;

void on_connection(uv_stream_t *server, int status);
void alloc_buffer(uv_handle_t *handle, size_t size, uv_buf_t *buf);
void on_read(uv_stream_t *client, ssize_t nread, const uv_buf_t *buf);
void on_write(uv_write_t *req, int status);

int
main(int argc, char *argv[])
{
    int r = 0;
    uv_tcp_t server;

    loop = uv_default_loop();

    uv_tcp_init(loop, &server);
    uv_ip4_addr("0.0.0.0", DEFAULT_PORT, &addr);
    uv_tcp_bind(&server, (const struct sockaddr *)&addr, 0);
    r = uv_listen((uv_stream_t *)&server, DEFAULT_BACKLOG, on_connection);

    if (r != 0) {
        fprintf(stderr, "error: listen error %s\n", uv_strerror(r));
        return 1;
    }

    return uv_run(loop, UV_RUN_DEFAULT);
}

void
on_connection(uv_stream_t *server, int status)
{
    uv_tcp_t *client = NULL;

    if (status < 0) {
        printf("error: new connection error - %s\n", uv_strerror(status));
        return;  /* error */
    }

    client = malloc(sizeof(*client));
    uv_tcp_init(loop, client);

    if (uv_accept(server, (uv_stream_t *)client) == 0) {
        uv_read_start((uv_stream_t *)client, alloc_buffer, on_read);
    } else {
        uv_close((uv_handle_t *)client, NULL);
    }
}

void
alloc_buffer(uv_handle_t *handle, size_t size, uv_buf_t *buf)
{
    *buf = uv_buf_init(malloc(size), size);
}

void
on_read(uv_stream_t *client, ssize_t nread, const uv_buf_t *buf)
{
    if (nread < 0) {
        if (nread != UV_EOF) {
            fprintf(stderr, "error: read error %s\n", uv_err_name(nread));
        }

        uv_close((uv_handle_t *)client, NULL);
    } else if (nread > 0) {
        uv_write_t *req = malloc(sizeof(*req));
        uv_buf_t wbuf = uv_buf_init(buf->base, nread);
        uv_write(req, client, &wbuf, 1, on_write);
    }

    if (buf->base) {
        free(buf->base);
    }

    uv_close((uv_handle_t *)client, NULL);
}

void
on_write(uv_write_t *req, int status)
{
    if (status < 0) {
        fprintf(stderr, "write error %s\n", uv_strerror(status));
    }

    free(req);
}

