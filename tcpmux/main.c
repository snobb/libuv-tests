/*
 *  main.c
 *  author: Aleksei Kozadaev (2015)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
#include <uv.h>

#define ERR(fmt, ...)   fprintf(stderr, "error: "fmt, __VA_ARGS__)
#define ERR0(fmt)       fprintf(stderr, "error: "fmt)
#define INFO(fmt, ...)  fprintf(stderr, "info: "fmt, __VA_ARGS__)

static const unsigned DEFAULT_PORT = 7878;
static const unsigned DEFAULT_BACKLOG = 255;
static const unsigned KEEPALIVE_TIMEOUT = 15;

struct qentry {
    uv_tcp_t *client;
    LIST_ENTRY(qentry) entries;
};

uv_loop_t *loop = NULL;
struct sockaddr_in addr;
LIST_HEAD(qhead, qentry) queue;

void on_connection(uv_stream_t *server, int status);
void alloc_buf(uv_handle_t *handle, size_t size, uv_buf_t *buf);
void on_read(uv_stream_t *client, ssize_t nread, const uv_buf_t *buf);
void on_write(uv_write_t *req, int status);
void on_timer(uv_timer_t *req);
void *xmalloc(size_t size);
void xfree(void *ptr);

int
main(int argc, char **argv)
{
    int res;
    uv_tcp_t server;
    uv_timer_t timer_req;
    loop = uv_default_loop();

    LIST_INIT(&queue);

    uv_timer_init(loop, &timer_req);
    uv_tcp_init(loop, &server);
    uv_ip4_addr("0.0.0.0", DEFAULT_PORT, &addr);
    uv_tcp_bind(&server, (const struct sockaddr*)&addr, 0);
    uv_tcp_keepalive(&server, 1, KEEPALIVE_TIMEOUT);

    res = uv_listen((uv_stream_t*)&server, DEFAULT_BACKLOG, on_connection);

    if (res != 0) {
        ERR("listen error %s\n", uv_strerror(res));
        return EXIT_FAILURE;
    }

    uv_timer_start(&timer_req, on_timer, 5000, 5000);

    return uv_run(loop, UV_RUN_DEFAULT);
}

void
on_connection(uv_stream_t *server, int status)
{
    uv_tcp_t *client = NULL;
    struct qentry *new = NULL;

    if (status < 0) {
        ERR("new connecton %s\n", uv_strerror(status));
        return;
    }

    client = xmalloc(sizeof(*client));
    uv_tcp_init(loop, client);

    if (uv_accept(server, (uv_stream_t*)client) == 0) {
        /* inserting the client to the queue */
        new = xmalloc(sizeof(*new));
        memset(new, 0, sizeof(*new));
        new->client = client;
        LIST_INSERT_HEAD(&queue, new, entries);

        /* setting up keepalive */
        uv_tcp_keepalive(client, 1, 5);

        INFO("new client %p\n", (void*)client);

        /* starting reading the data */
        uv_read_start((uv_stream_t*)client, alloc_buf, on_read);
    } else {
        uv_close((uv_handle_t*)client, NULL);
        xfree(client);
    }
}

void
alloc_buf(uv_handle_t *handle, size_t size, uv_buf_t *buf)
{
    static char buffer[1024] = { 0 };
    *buf = uv_buf_init(buffer, sizeof(buffer));
}

void
on_read(uv_stream_t *client, ssize_t nread, const uv_buf_t *buf)
{
    uv_write_t *wreq = NULL;
    struct qentry *qe = NULL;

    if (nread < 0) {
        if (nread != UV_EOF) {
            ERR("read error %s\n", uv_err_name(nread));
        }
    } else if (nread > 0) {
        uv_buf_t wbuf = uv_buf_init(buf->base, nread);
        LIST_FOREACH(qe, &queue, entries) {
            if (qe->client && (uv_stream_t *)qe->client != client) {
                wreq = xmalloc(sizeof(*wreq));
                INFO("writing to client %p\n", (void*)qe->client);
                uv_write(wreq, (uv_stream_t*)qe->client, &wbuf, 1, on_write);
            }
        }
    }
}

void
on_write(uv_write_t *req, int status)
{
    if (status < 0) {
        ERR("write error %s\n", uv_strerror(status));
    }
    xfree(req);
}

void
on_timer(uv_timer_t *req)
{
    struct qentry *qe = NULL;

    LIST_FOREACH(qe, &queue, entries) {
        if (!uv_is_active((uv_handle_t *)qe->client)) {
            INFO("cleaning client %p\n", (void*)qe->client);

            LIST_REMOVE(qe, entries);
            xfree(qe->client);
            xfree(qe);
        }
    }
}

void *
xmalloc(size_t size)
{
    void *buf = malloc(size);

    if (!buf) {
        ERR0("malloc failure\n");
        exit(EXIT_FAILURE);
    }

    return buf;
}

void
xfree(void *ptr)
{
    free(ptr);
}
