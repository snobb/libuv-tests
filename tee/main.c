/*
 *  main.c
 *  author: Aleksei Kozadaev (2015)
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <uv.h>

typedef struct {
    uv_write_t req;
    uv_buf_t buf;
} write_req_t;

static uv_loop_t *loop = NULL;

static uv_pipe_t stdin_pipe;
static uv_pipe_t stdout_pipe;
static uv_pipe_t file_pipe;

void alloc_buffer(uv_handle_t *handle, size_t suggested_size,
                  uv_buf_t *buf);
void read_stdin(uv_stream_t *stream, ssize_t nread, const uv_buf_t *buf);
void write_data(uv_stream_t *dest, size_t size, uv_buf_t buf,
                uv_write_cb db);
void free_write_req(uv_write_t *req);
void on_file_write(uv_write_t *req, int status);
void on_stdout_write(uv_write_t *req, int status);

int
main(int argc, char *argv[])
{
    uv_fs_t file_req;
    int fd;

    if (argc < 2) {
        printf("usage: %s <file>\n", argv[0]);
        exit(1);
    }

    signal(SIGPIPE, SIG_IGN);
    loop = uv_default_loop();

    uv_pipe_init(loop, &stdin_pipe, 0);
    uv_pipe_open(&stdin_pipe, 0);

    uv_pipe_init(loop, &stdout_pipe, 0);
    uv_pipe_open(&stdout_pipe, 1);

    fd = uv_fs_open(loop, &file_req, argv[1], O_CREAT | O_RDWR, 0644, NULL);

    uv_pipe_init(loop, &file_pipe, 0);
    uv_pipe_open(&file_pipe, fd);

    uv_read_start((uv_stream_t *)&stdin_pipe, alloc_buffer, read_stdin);

    return uv_run(loop, UV_RUN_DEFAULT);
}


void
alloc_buffer(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf)
{
    *buf = uv_buf_init(malloc(suggested_size), suggested_size);
}

void
read_stdin(uv_stream_t *stream, ssize_t nread, const uv_buf_t *buf)
{
    if (nread < 0) {
        if (nread == EOF) {
            uv_close((uv_handle_t *)&stdin_pipe, NULL);
            uv_close((uv_handle_t *)&stdout_pipe, NULL);
            uv_close((uv_handle_t *)&file_pipe, NULL);
        }
    } else if (nread > 0) {
        /* got some data - writing */
        write_data((uv_stream_t *)&file_pipe, nread, *buf, on_file_write);
        write_data((uv_stream_t *)&stdout_pipe, nread, *buf, on_stdout_write);
    }

    if (buf->base) {
        free(buf->base);
    }
}

void
write_data(uv_stream_t *dest, size_t size, uv_buf_t buf, uv_write_cb cb)
{
    write_req_t *req = malloc(sizeof(*req));
    req->buf = uv_buf_init(malloc(size), size);
    memcpy(req->buf.base, buf.base, size);
    uv_write((uv_write_t *)req, dest, &req->buf, 1, cb);
}

void
free_write_req(uv_write_t *req)
{
    write_req_t *wr = (write_req_t*)req;
    free(wr->buf.base);
    free(wr);
}

void
on_file_write(uv_write_t *req, int status)
{
    free_write_req(req);
}

void
on_stdout_write(uv_write_t *req, int status)
{
    free_write_req(req);
}

