#!/usr/bin/env bash
#
# libuv_init.sh
# Author: Alex Kozadaev (2015)
#

if [[ ! -d libuv ]]; then
    git clone https://github.com/libuv/libuv.git
else
    pushd libuv
    git pull
    popd
fi

pushd libuv
    ./autogen.sh && \
    ./configure && \
    make -j4
popd


