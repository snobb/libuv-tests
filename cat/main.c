/*
 *  main.c
 *  Author: Alex Kozadaev (2015)
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <uv.h>

static uv_fs_t open_req;
static uv_fs_t read_req;
static uv_fs_t write_req;

static char buffer[BUFSIZ];
static uv_buf_t iov;

void on_open(uv_fs_t *req);
void on_read(uv_fs_t *req);
void on_write(uv_fs_t *req);

int
main(int argc, char *argv[])
{
    int ret = EXIT_SUCCESS;

    uv_fs_open(uv_default_loop(), &open_req, argv[1], O_RDONLY, 0, on_open);
    uv_run(uv_default_loop(), UV_RUN_DEFAULT);

    uv_fs_req_cleanup(&open_req);
    uv_fs_req_cleanup(&read_req);
    uv_fs_req_cleanup(&write_req);
    return ret;
}

void
on_open(uv_fs_t *req)
{
    /* req should be the same as the one passed to uv_fs_open */
    assert(req == &open_req);

    if (req->result < 0) {
        fprintf(stderr, "error opening file: %s\n",
                uv_strerror((int)req->result));
    } else {
        iov = uv_buf_init(buffer, sizeof(buffer));
        uv_fs_read(uv_default_loop(), &read_req, req->result, &iov, 1, -1,
                   on_read);
    }
}

void
on_read(uv_fs_t *req)
{
    if (req->result < 0) {
        fprintf(stderr, "error reading file: %s\n",
                uv_strerror((int)req->result));
    } else if (req->result == 0) {
        uv_fs_t close_req;
        /* no callback - syncronous */
        uv_fs_close(uv_default_loop(), &close_req, open_req.result, NULL);
    } else if (req->result > 0) {
        iov.len = req->result;
        /* 1 - stdout */
        uv_fs_write(uv_default_loop(), &write_req, 1, &iov, 1, -1, on_write);
    }
}

void
on_write(uv_fs_t *req)
{
    if (req->result < 0) {
        fprintf(stderr, "output error: %s\n", uv_strerror((int)req->result));
    } else {
        uv_fs_read(uv_default_loop(), &read_req, open_req.result,
                   &iov, 1, -1, on_read);
    }
}

