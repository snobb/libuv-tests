/*
 *  main.c
 *  Author: Alex Kozadaev (2015)
 */

#include <stdio.h>
#include <stdlib.h>
#include <uv.h>

static uv_loop_t *loop = NULL;
static char *command = NULL;

static void on_run_command(uv_fs_event_t *event_handle,
                           const char *fname, int events, int status);

int
main(int argc, char *argv[])
{
    loop = uv_default_loop();

    if (argc <= 2) {
        printf("%s v%s\n", argv[0], VERSION);
        printf("usage: %s <command> <file1> <file2> ...\n", argv[0]);
        exit(1);
    }

    command = argv[1];

    while (argc-- > 2) {
        uv_fs_event_t *event_handle = malloc(sizeof(*event_handle));
        uv_fs_event_init(loop, event_handle);
        uv_fs_event_start(event_handle, on_run_command,
                          argv[argc], UV_FS_EVENT_WATCH_ENTRY);
    }

    return uv_run(loop, UV_RUN_DEFAULT);
}

void
on_run_command(uv_fs_event_t *event_handle, const char *fname,
               int events, int status)
{
    /*
     *  full path of the changed file can be found by running
     *  uv_fs_event_getpath(...) from event_handle
     */

    if (events & UV_RENAME) {
        fprintf(stderr, "the file %s has been renamed\n", fname);
    } else if (events & UV_CHANGE) {
        fprintf(stderr, "the file %s has been changed\n", fname);
    }

    system(command);
}

