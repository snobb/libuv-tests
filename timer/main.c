/*
 *  main.c
 *  Author: Alex Kozadaev (2015)
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <uv.h>

void timer_cb(uv_timer_t *timer_req);

int main(int argc, char *argv[])
{
    uv_loop_t *loop = uv_default_loop();
    uv_timer_t timer_req;

    if (!loop) {
        fputs("error: allocation failure", stderr);
        goto error;
    }

    uv_timer_init(loop, &timer_req);

    /* second parameter should be zero to prevent repeating.
     * in case of zero - loop will exit after first callback run */
    uv_timer_start(&timer_req, timer_cb, 2000, 1000);

    return uv_run(loop, UV_RUN_DEFAULT);
}

void timer_cb(uv_timer_t *timer_req)
{
    time_t rawtime;
    time(&rawtime);
    printf("timer: %s", ctime(&rawtime));
}

