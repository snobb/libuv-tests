/*
 *  main.c
 *  author: Aleksei Kozadaev (2015)
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include "uv.h"

uv_loop_t *loop = NULL;
void on_resolve(uv_getaddrinfo_t *req, int status, struct addrinfo *res);

int
main(int argc, char **argv)
{
    struct addrinfo hints;
    uv_getaddrinfo_t resolver;
    int res;
    const char *node = "f5.com";

    loop = uv_default_loop();

    hints.ai_family = PF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;
    hints.ai_flags = 0;

    fprintf(stderr, "resolving %s ... ", node);
    res = uv_getaddrinfo(loop, &resolver, on_resolve, node, NULL, &hints);

    if (res) {
        fprintf(stderr, "uv_getaddrinfo call error %s", uv_err_name(res));
        return 1;
    }

    return uv_run(loop, UV_RUN_DEFAULT);
}

void
on_resolve(uv_getaddrinfo_t *req, int status, struct addrinfo *res)
{
    if (status < 0) {
        fprintf(stderr, "uv_getaddrinfo call error %s", uv_err_name(status));
        return;
    }

    char addr[17] = { 0 };
    uv_ip4_name((struct sockaddr_in *)res->ai_addr, addr, 16);
    fprintf(stderr, "%s\n", addr);

    uv_freeaddrinfo(res);
}

